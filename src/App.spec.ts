/**
 * @vitest-environment happy-dom
 */
import { nextTick } from "vue";
import { describe, expect, it } from "vitest";
import "@testing-library/jest-dom";
import { render } from "@testing-library/vue";
import { createTestingPinia } from "@pinia/testing";
import { buildActivity } from "/@/factories/activity";
import { useActivities } from "/@/stores/activities";
import { mockLeaflet } from "/@/testUtils";
mockLeaflet();
import App from "./App.vue";

describe("App", () => {
  it("shows a list of markers for each activity", async () => {
    const { getByText } = render(App, {
      global: { plugins: [createTestingPinia()] },
    });
    const activities = useActivities();
    activities.list = [
      buildActivity(1, "Cross-country skiing"),
      buildActivity(2, "Ski touring in the Alps"),
    ];
    await nextTick();
    expect(getByText("Cross-country skiing")).toBeVisible();
    expect(getByText("Ski touring in the Alps")).toBeVisible();
  });
});
