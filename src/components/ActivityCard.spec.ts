/**
 * @vitest-environment happy-dom
 */
import { describe, it } from "vitest";
import "@testing-library/jest-dom";
import { render } from "@testing-library/vue";
import ActivityCard from "./ActivityCard.vue";
import { buildActivity } from "/@/factories/activity";

describe("ActivityCard", () => {
  it("displays the title of the activity", () => {
    const { getByRole } = render(ActivityCard, {
      props: { activity: buildActivity(1, "A winter activity") },
    });
    expect(
      getByRole("heading", { name: "A winter activity", level: 3 })
    ).toBeVisible();
  });

  it("displays the image for the activity", () => {
    const { getByRole } = render(ActivityCard, {
      props: {
        activity: buildActivity(1, "A winter activity", {
          image: {
            url: "https://www.example.org/image.jpg",
            caption: "Beautiful snowy mountains",
          },
        }),
      },
    });
    const image = getByRole("img", { name: "Beautiful snowy mountains" });
    expect(image).toBeVisible();
    expect(image).toHaveAttribute("src", "https://www.example.org/image.jpg");
  });
});
