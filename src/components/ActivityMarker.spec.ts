/**
 * @vitest-environment happy-dom
 */
import { describe, expect, it } from "vitest";
import "@testing-library/jest-dom";
import { render } from "@testing-library/vue";
import { mockLeaflet } from "/@/testUtils";
mockLeaflet();
import ActivityMarker from "./ActivityMarker.vue";

describe("ActivityMarker", () => {
  it("displays elements within the popup of a map marker", () => {
    const { getByText, container } = render(ActivityMarker, {
      slots: { default: "Some content" },
      props: { latitude: 2, longitude: 5 },
    });

    const content = getByText("Some content");
    expect(content).toBeVisible();

    // Normally this should be avoided, however this component is basically
    // wrapping leaflet, so we test the HTML structure as well.
    const markerElement = container.querySelector("lmarker-stub");
    expect(markerElement).toHaveAttribute("lat-lng", "2,5");
    const popupElement = markerElement?.firstChild;
    expect(popupElement).toContainHTML("Some content");
  });
});
