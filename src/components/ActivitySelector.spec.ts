/**
 * @vitest-environment happy-dom
 */
import { createTestingPinia } from "@pinia/testing";
import "@testing-library/jest-dom";
import { fireEvent, render } from "@testing-library/vue";
import { describe, it } from "vitest";
import { nextTick } from "vue";
import { useActivities } from "../stores/activities";
import ActivitySelector from "./ActivitySelector.vue";

describe("ActivitySelector", () => {
  it("loads skiing activities by default", async () => {
    const { getByLabelText } = render(ActivitySelector, {
      global: { plugins: [createTestingPinia()] },
    });
    await nextTick();
    const activities = useActivities();
    expect(activities.load).toHaveBeenCalledWith("ski");
    expect(getByLabelText("Skiing and Snowboarding")).toBeChecked();
  });

  it("loads hiking activities when clicking on the button", async () => {
    const { getByLabelText } = render(ActivitySelector, {
      global: { plugins: [createTestingPinia()] },
    });
    const activities = useActivities();
    const hikingButton = getByLabelText("Hiking");
    await fireEvent.update(hikingButton);
    expect(activities.load).toHaveBeenCalledWith("hiking");
    expect(hikingButton).toBeChecked();
    expect(getByLabelText("Skiing and Snowboarding")).not.toBeChecked();
  });

  it("loads skiing activities when clicking on the button", async () => {
    const { getByLabelText } = render(ActivitySelector, {
      global: { plugins: [createTestingPinia()] },
    });
    const activities = useActivities();

    await fireEvent.update(getByLabelText("Hiking"));
    await fireEvent.update(getByLabelText("Skiing and Snowboarding"));
    expect(activities.load).toHaveBeenNthCalledWith(3, "ski");
  });
});
