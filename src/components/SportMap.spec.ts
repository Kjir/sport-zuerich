/**
 * @vitest-environment happy-dom
 */
import { describe, expect, it } from "vitest";
import "@testing-library/jest-dom";
import { render } from "@testing-library/vue";
import { mockLeaflet } from "/@/testUtils";
mockLeaflet();
import SportMap from "./SportMap.vue";

describe("SportMap", () => {
  it("displays elements within a map", () => {
    const { getByText, container } = render(SportMap, {
      slots: { default: "Some content" },
    });

    const content = getByText("Some content");
    expect(content).toBeVisible();

    // Normally this should be avoided, however this component is basically
    // wrapping leaflet, so we test the HTML structure as well.
    const mapElement = container.querySelector("lmap-stub");
    expect(mapElement).toContainHTML("Some content");
  });
});
