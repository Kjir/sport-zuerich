import { Activity } from "/@/model/activity";

type OptionalActivityData = {
  image?: { url: string; caption: string };
};
export function buildActivity(
  identifier: string | number,
  name = "test activity",
  { image }: OptionalActivityData = {}
) {
  let activity: Activity = {
    identifier,
    name,
    geoLocation: { longitude: 4.342, latitude: 2.3456 },
  };
  if (image) {
    activity.image = image;
  }
  return activity;
}
