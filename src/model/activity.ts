export type Activity = {
  identifier: number | string;
  name: string;
  geoLocation: {
    latitude: number;
    longitude: number;
  };
  image?: {
    url: string;
    caption: string;
  };
};
