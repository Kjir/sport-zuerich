import { beforeAll, beforeEach, describe, expect, it, vi } from "vitest";
import { createPinia, setActivePinia } from "pinia";
import { useActivities } from "./activities";
import { buildActivity } from "/@/factories/activity";

const skiActivities = vi.fn().mockReturnValue([]);
const hikingActivities = vi.fn().mockReturnValue([]);

describe("activities", () => {
  beforeEach(() => {
    setActivePinia(createPinia());
  });

  beforeAll(mockSkiModule);
  beforeAll(mockHikingModule);
  beforeEach(() => {
    skiActivities.mockClear().mockReturnValue([]);
    hikingActivities.mockClear().mockReturnValue([]);
  });

  it("starts with an empty list of activities", () => {
    const activities = useActivities();
    expect(activities.list).toEqual([]);
  });

  it("loads skiing activities", async () => {
    const id = generateId();
    skiActivities.mockReturnValue([buildAPIActivity(id, "abc")]);
    const activities = useActivities();
    await activities.load("ski");
    expect(activities.list).toEqual([buildActivity(id, "abc")]);
  });

  it("loads hiking activities", async () => {
    const skiId = generateId();
    const hikeId = generateId();
    skiActivities.mockReturnValue([buildAPIActivity(skiId, "Snow")]);
    hikingActivities.mockReturnValue([buildAPIActivity(hikeId, "Hike")]);
    const activities = useActivities();
    await activities.load("hiking");
    expect(activities.list).toEqual([buildActivity(hikeId, "Hike")]);
  });

  it("counts the activities", async () => {
    const [id1, id2, id3] = [generateId(), generateId(), generateId()];
    skiActivities.mockReturnValue([
      buildAPIActivity(id1, "abc"),
      buildAPIActivity(id2, "cde"),
      buildAPIActivity(id3, "efg"),
    ]);
    const activities = useActivities();
    await activities.load("ski");
    expect(activities.count).toEqual(3);
  });

  it("treats empty values as an empty list", async () => {
    vi.spyOn(console, "error").mockImplementation(() => {});
    skiActivities.mockReturnValue(null);

    const activities = useActivities();
    await activities.load("ski");

    expect(activities.list).toEqual([]);
    expect(console.error).not.toHaveBeenCalled();
  });

  it("notifies about wrong api responses", async () => {
    vi.spyOn(console, "error").mockImplementation(() => {});
    skiActivities.mockReturnValue({});

    const activities = useActivities();
    await activities.load("ski");

    expect(activities.list).toEqual([]);
    expect(console.error).toHaveBeenCalledWith(
      "Returned activities are not in the correct format",
      {}
    );
  });

  it("excludes empty activities", async () => {
    vi.spyOn(console, "error").mockImplementation(() => {});
    const id = generateId();
    const validActivity = buildAPIActivity(id, "some activity");
    skiActivities.mockReturnValue([null, validActivity]);

    const activities = useActivities();
    await activities.load("ski");

    expect(activities.list).toEqual([buildActivity(id, "some activity")]);
    expect(console.error).not.toHaveBeenCalled();
  });

  it("excludes activities in the wrong format with an error notification", async () => {
    vi.spyOn(console, "error").mockImplementation(() => {});
    const id = generateId();
    const validActivity = buildAPIActivity(id);
    skiActivities.mockReturnValue([
      validActivity,
      "wrongly formatted activity",
    ]);

    const activities = useActivities();
    await activities.load("ski");

    expect(activities.list).toEqual([buildActivity(id)]);
    expect(console.error).toHaveBeenCalledWith(
      "Activity has the wrong format",
      "wrongly formatted activity"
    );
  });

  it("excludes activities missing an english name", async () => {
    vi.spyOn(console, "error").mockImplementation(() => {});
    const activity1 = { name: { de: "Englisch nicht verfügbar" } };
    const activity2 = {};
    skiActivities.mockReturnValue([activity1, activity2]);

    const activities = useActivities();
    await activities.load("ski");

    expect(activities.list).toEqual([]);
    expect(console.error).toHaveBeenCalledWith(
      "Activity is missing the name",
      activity1
    );
    expect(console.error).toHaveBeenCalledWith(
      "Activity is missing the name",
      activity2
    );
  });

  it("excludes activities missing a geo location", async () => {
    vi.spyOn(console, "error").mockImplementation(() => {});
    const activity1 = { name: { en: "Without geolocation" } };
    const activity2 = {
      name: { en: "Without longitude" },
      geoLocation: { latitude: 1.234 },
    };
    const activity3 = {
      name: { en: "Without latitude" },
      geoLocation: { longitude: 1.234 },
    };
    skiActivities.mockReturnValue([activity1, activity2, activity3]);

    const activities = useActivities();
    await activities.load("ski");

    expect(activities.list).toEqual([]);
    expect(console.error).toHaveBeenCalledWith(
      "Activity is missing a geographic location",
      activity1
    );
    expect(console.error).toHaveBeenCalledWith(
      "Activity is missing a geographic location",
      activity2
    );
    expect(console.error).toHaveBeenCalledWith(
      "Activity is missing a geographic location",
      activity3
    );
  });

  it("excludes activities with an invalid geo location", async () => {
    vi.spyOn(console, "error").mockImplementation(() => {});
    const activity1 = {
      name: { en: "Longitude is NaN" },
      geoLocation: { latitude: 1.234, longitude: "cvxz" },
    };
    const activity2 = {
      name: { en: "Latitude is NaN" },
      geoLocation: { longitude: 1.234, latitude: "abc" },
    };
    skiActivities.mockReturnValue([activity1, activity2]);

    const activities = useActivities();
    await activities.load("ski");

    expect(activities.list).toEqual([]);
    expect(console.error).toHaveBeenCalledWith(
      "Activity is missing a geographic location",
      activity1
    );
    expect(console.error).toHaveBeenCalledWith(
      "Activity is missing a geographic location",
      activity2
    );
  });

  it("includes the geo coordinates", async () => {
    const id = generateId();
    skiActivities.mockReturnValue([
      buildAPIActivity(id, "abc", {
        latitude: 2.123,
        longitude: 3.789,
      }),
    ]);
    const activities = useActivities();
    await activities.load("ski");
    expect(activities.list).toHaveLength(1);
    expect(activities.list[0]).toHaveProperty("geoLocation", {
      latitude: 2.123,
      longitude: 3.789,
    });
  });
  it("includes the image if the URL is available", async () => {
    const id = generateId();
    skiActivities.mockReturnValue([
      buildAPIActivity(id, "abc", {
        image: buildImage("https://www.example.com/image.jpg"),
      }),
    ]);
    const activities = useActivities();
    await activities.load("ski");
    expect(activities.list).toHaveLength(1);
    expect(activities.list[0]).toHaveProperty("image", {
      url: "https://www.example.com/image.jpg",
      caption: "",
    });
  });

  it("includes the caption for the image if available", async () => {
    const id = generateId();
    skiActivities.mockReturnValue([
      buildAPIActivity(id, "abc", {
        image: buildImage("https://www.example.com/image.jpg", "some caption"),
      }),
    ]);
    const activities = useActivities();
    await activities.load("ski");
    expect(activities.list).toHaveLength(1);
    expect(activities.list[0]).toHaveProperty("image", {
      url: "https://www.example.com/image.jpg",
      caption: "some caption",
    });
  });
});

function mockSkiModule() {
  vi.mock("/@/api/ski.json", () => ({
    get default() {
      return skiActivities();
    },
  }));
}

function mockHikingModule() {
  vi.mock("/@/api/hiking.json", () => ({
    get default() {
      return hikingActivities();
    },
  }));
}
type APIImage = { url: string; caption: { en?: string | null } };
type OptionalBuildData = {
  image?: APIImage;
  latitude?: number;
  longitude?: number;
};
function buildAPIActivity(
  identifier: string | number,
  name = "test activity",
  { image, latitude = 2.3456, longitude = 4.342 }: OptionalBuildData = {}
) {
  const activity: any = {
    identifier,
    name: { en: name },
    geoCoordinates: { longitude, latitude },
  };
  if (image) {
    activity.image = image;
  }
  return activity;
}

function buildImage(url: string, caption: string | null = null) {
  return { url, caption: { en: caption } };
}

function generateId() {
  return Math.floor(Math.random() * 10000);
}
