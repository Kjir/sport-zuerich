import { computed, Ref, ref } from "vue";
import { defineStore } from "pinia";
import { Activity } from "/@/model/activity";

export const useActivities = defineStore("activities", () => {
  const list: Ref<Activity[]> = ref([]);
  async function load(activityType: "ski" | "hiking") {
    switch (activityType) {
      // The values come from the API, but since no CORS headers are set, they
      // are loaded from a local json file instead.
      case "ski":
        // const response = await fetch("https://www.zuerich.com/de/data?id=152");
        list.value = parseActivities((await import("/@/api/ski.json")).default);
        break;
      case "hiking":
        // const response = await fetch("https://www.zuerich.com/de/data?id=138");
        list.value = parseActivities(
          (await import("/@/api/hiking.json")).default
        );
        break;
      default:
        list.value = [];
    }
  }

  const count = computed(() => {
    return list.value.length;
  });

  return { list, load, count };
});

function parseActivities(activities: unknown) {
  if (!activities) {
    return [];
  }
  if (!Array.isArray(activities)) {
    notifyError(
      "Returned activities are not in the correct format",
      activities
    );
    return [];
  }
  return activities
    .map(parseActivity)
    .filter((activity): activity is Activity => !!activity);
}

function parseActivity(activity: any): Activity | null {
  if (!activity) {
    return null;
  }
  if (typeof activity != "object") {
    notifyError("Activity has the wrong format", activity);
    return null;
  }
  if (!("name" in activity) || !activity.name.en) {
    notifyError("Activity is missing the name", activity);
    return null;
  }

  if (
    !activity.geoCoordinates ||
    !activity.geoCoordinates.latitude ||
    !activity.geoCoordinates.longitude ||
    isNaN(activity.geoCoordinates.latitude) ||
    isNaN(activity.geoCoordinates.longitude)
  ) {
    notifyError("Activity is missing a geographic location", activity);
    return null;
  }
  let parsedActivity: Activity = {
    identifier: activity.identifier || Math.floor(Math.random() * 10000),
    name: `${activity.name.en}`,
    geoLocation: {
      latitude: +activity.geoCoordinates.latitude,
      longitude: +activity.geoCoordinates.longitude,
    },
  };
  if (activity.image?.url) {
    parsedActivity.image = {
      url: `${activity.image.url}`,
      caption: activity.image.caption?.en ? `${activity.image.caption.en}` : "",
    };
  }
  return parsedActivity;
}

function notifyError(error: string, dataWithProblems: any) {
  // Here it could do something more fancy to display a user-friendly error.
  // For simplicity we just log it to the console.
  console.error(error, dataWithProblems);
}
