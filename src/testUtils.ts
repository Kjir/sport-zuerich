import { vi } from "vitest";

export function mockLeaflet() {
  vi.mock("@vue-leaflet/vue-leaflet", () => ({
    LMap: "lmap-stub",
    LTileLayer: "ltile-stub",
    LMarker: "lmarker-stub",
    LPopup: "lpopup-stub",
  }));
}
